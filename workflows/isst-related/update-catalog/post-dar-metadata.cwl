cwlVersion: v1.2
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: gitlab.lcsb.uni.lu:4567/luca.bolzani/iderha-test-deployment/iderha-mdc-management

inputs:
  DARMetadataFiles:
    type:
      type: array
      items: File
      inputBinding:
        prefix: --file
    inputBinding:
      position: 1

  locations:
    type:
      type: array
      items: string
      inputBinding:
        prefix: --location
    inputBinding:
      position: 2

outputs: []

baseCommand: "python3"
arguments:
  - "/app/post-dar-metadata.py"

#  - -X
#  - POST
#  - -H
#  - Content-Type:application/json
#  - --location
#  - http://iderha-catalog:3000/api/catalog
#  - --data
#  - @$(inputs.assetsMetadata.path)