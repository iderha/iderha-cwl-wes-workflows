cwlVersion: v1.2
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  jsonTemplate:
    type: File
  location1:
    type: string
    default:
outputs: []

steps:
  getMetaData1:
    in:
      jsonTemplate:
        source: [jsonTemplate]
      location:
        valueFrom: "http://tesk-api-node-1:8080/ga4gh/tes"

    out: [assetsMetadata]
    run: get-assets-metadata.cwl

  getMetadata2:
    in:
      jsonTemplate:
        source: [jsonTemplate]
      location:
        valueFrom: "http://tesk-api-node-2:8080/ga4gh/tes"

    out: [assetsMetadata]
    run: get-assets-metadata.cwl

  updateCatalog:
    in:
      assetsMetadataFiles:
        source: [getMetaData1/assetsMetadata, getMetadata2/assetsMetadata]
        linkMerge: merge_nested
    out: []
    run: post-assets-metadata.cwl

