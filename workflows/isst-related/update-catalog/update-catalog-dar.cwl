cwlVersion: v1.2
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}

inputs:
  location:
    type: string
    default: "http://tesk-api-node-1:8080/ga4gh/tes"

  location2:
    type: string
    default: "http://tesk-api-node-2:8080/ga4gh/tes"

outputs: []

steps:
  getDAR1:
    in:
      location:
        source: [location]

    out: [DARMetadata, location]
    run: get-dar-metadata.cwl

  getDAR2:
    in:
      location:
        source: [location2]

    out: [DARMetadata, location]
    run: get-dar-metadata.cwl

  updateCatalog:
    in:
      DARMetadataFiles:
        source: [getDAR1/DARMetadata, getDAR2/DARMetadata]
        linkMerge: merge_nested
      locations:
        source: [getDAR1/location, getDAR2/location]
        linkMerge: merge_nested
    out: []
    run: post-dar-metadata.cwl

