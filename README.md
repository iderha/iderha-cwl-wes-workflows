# iderha-cwl-wes-workflows



## Getting started

Check examples inside [integration_tests.sh](integration_tests.sh) 

To create a DRS object or a TRS tool through a post request, drs-object-example.json and trs-tool-example.json can be used in the json body of the request.