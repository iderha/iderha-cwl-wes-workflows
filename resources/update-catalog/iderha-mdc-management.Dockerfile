FROM repomanager.lcsb.uni.lu:9999/python:3.9

WORKDIR /app

RUN python3 -m pip install requests --no-cache-dir

COPY get-assets-metadata.py .
COPY post-assets-metadata.py .
COPY post-dar-metadata.py .
